var Fruit = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.setAnchorPoint( new cc.p( 0, 0 ));
        this.speed = Math.random()*4 +2;
    },
    
    update: function(dt) {
        var posFruit = this.getPosition();

        this.setPosition(new cc.p(posFruit.x, posFruit.y - this.speed));
        if(posFruit.y < -100) {
           this.setPosition( new cc.p(Math.floor(Math.random() * screenWidth),screenHeight));
        } 
    },
    
    hit: function( penguin ) {
        var penguinPos = penguin.getPosition();
        var fruitPos = this.getPosition();
        
        return this.checkFruitCollision( penguinPos.x, penguinPos.y, fruitPos.x, fruitPos.y );
    },
    
    checkFruitCollision: function( penguinPosx, penguinPosy, fruitPosx, fruitPosy ) {
        if( fruitPosx >= penguinPosx-40 && fruitPosx <= penguinPosx+10  && fruitPosy <= penguinPosy+10 && fruitPosy >= 0 ) {
             score++;
             cc.audioEngine.stopAllEffects();
             cc.audioEngine.playEffect( res.catchFruit );
             this.setPosition( new cc.p(Math.floor(Math.random() * screenWidth),1200));
        }
        return true;   
    },
});
