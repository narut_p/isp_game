var MainMenu = cc.Layer.extend({
    ctor: function() {
        this._super();
        this.init();
    },
    
    init: function() {
        this._super();  
        cc.audioEngine.playMusic( res.MainmenuMusic );
        var bg = cc.Sprite.create( res.BGMain );
        bg.setPosition( new cc.p( 400, 300 ));
        this.addChild( bg );
        
        this.dragon = new Dragon2();
        this.dragon.setPosition( new cc.p(0,450));
        this.addChild( this.dragon);
        this.dragon.scheduleUpdate();
        
        this.scheduleUpdate();
        this.addKeyboardHandlers();
        
        return true;
    },
    
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    onKeyDown: function( keyCode, event ) {
        
    },
    
    onKeyUp: function( keyCode, event ) {
        if (keyCode == cc.KEY.enter) {
            live = 3;
            score = 0;
            cc.director.runScene(new InstructionScene());
        }
    },
    
    update: function(dt) {

    }
});

MainMenu.DIR = {
    START: 1,
}

var MainScene = cc.Scene.extend({
   onEnter: function() {
       this._super();
       var layer = new MainMenu();
       layer.init();
       this.addChild( layer );
   }
});