var Dragon = cc.Sprite.extend({
    ctor: function() {
        this._super();
   
        var animation = new cc.Animation.create();
    animation.addSpriteFrameWithFile( 'res/Dragon/DragonFly0.png' );
	animation.addSpriteFrameWithFile( 'res/Dragon/DragonFly1.png' );
	animation.addSpriteFrameWithFile( 'res/Dragon/DragonFly2.png' );
    animation.addSpriteFrameWithFile( 'res/Dragon/DragonFly3.png' );
    animation.addSpriteFrameWithFile( 'res/Dragon/DragonFly4.png' );
    animation.addSpriteFrameWithFile( 'res/Dragon/DragonFly5.png' );
	animation.setDelayPerUnit( 0.15 );

    var movingAction = cc.RepeatForever.create( cc.Animate.create( animation ) );
	this.runAction( movingAction );
   
    },
    
    update: function(dt) {
    var posDragon = this.getPosition();
    this.setPosition( new cc.p(posDragon.x - 0.2, posDragon.y ));
    if(posDragon.x < -160) {
        this.setPosition( new cc.p(800, 350 + Math.floor(Math.random() * (screenHeight - 350))));
    }    
 },
    
});