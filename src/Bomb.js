var heart = [];
var Bomb = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/Bomb.png' );
        this.setAnchorPoint( new cc.p( 0, 0 ));
        
        this.speed = Math.random()*5 +2;
    },
    
    update: function(dt) {
    this.initWithFile( 'res/Bomb.png' );
    var posBomb = this.getPosition();
    this.setPosition(new cc.p(posBomb.x, posBomb.y - this.speed));
    if(posBomb.y <-10) {
        this.setPosition( new cc.p(Math.floor(Math.random() * screenWidth),600));
    } 
   },

    hit: function( penguin ) {
        var penguinPos = penguin.getPosition();
        var bombPos = this.getPosition();
        
        return this.checkBallCollision( penguinPos.x, penguinPos.y, bombPos.x, bombPos.y );
    },
    
    checkBallCollision: function( penguinPosx, penguinPosy, bombPosx, bombPosy ) {
        
        if( live >= 0 ) {
            
            if(bombPosx >= penguinPosx-40 && bombPosx <= penguinPosx+10  && bombPosy <= penguinPosy+10 && bombPosy >= 0) {
                live--;
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( res.catchBomb );
                this.setPosition( new cc.p(Math.floor(Math.random() * screenWidth) , 700));
                heart.pop().removeFromParent();
                
            }
        }
        else {
            live = 3;
        }     
        return true;   
    },
    
});
