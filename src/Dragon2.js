var Dragon2 = cc.Sprite.extend({
    ctor: function() {
        this._super();
   
        var animation = new cc.Animation.create();
    animation.addSpriteFrameWithFile( 'res/Dragon/Dragon1.png' );
	animation.addSpriteFrameWithFile( 'res/Dragon/Dragon2.png' );
	animation.addSpriteFrameWithFile( 'res/Dragon/Dragon3.png' );
    animation.addSpriteFrameWithFile( 'res/Dragon/Dragon4.png' );
    animation.addSpriteFrameWithFile( 'res/Dragon/Dragon5.png' );
    animation.addSpriteFrameWithFile( 'res/Dragon/Dragon6.png' );
	animation.setDelayPerUnit( 0.15 );

    var movingAction = cc.RepeatForever.create( cc.Animate.create( animation ) );
	this.runAction( movingAction );
   
    },
    
    update: function(dt) {
    var posDragon = this.getPosition();
    this.setPosition( new cc.p(posDragon.x + 0.2, posDragon.y ));
    if(posDragon.x > 960) {
        this.setPosition( new cc.p(0, 350 + Math.floor(Math.random() * (screenHeight - 350))));
    }    
 },
    
});