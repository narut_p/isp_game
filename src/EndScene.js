var EndScene = cc.Layer.extend({
    ctor: function() {
        this._super();
        this.init();
    },
    
    init: function() {
        this._super();
        cc.audioEngine.playMusic( res.GameOverSound );
        var bg = cc.Sprite.create( res.GameOver );
        bg.setPosition( new cc.p( 400, 300 ));
        this.addChild( bg );
        this.scheduleUpdate();
        this.addKeyboardHandlers();
        if( score >= highscore ) {
        highscore = score;
        }
        return true;
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    onKeyDown: function( keyCode, event ) {
        
    },
    
    onKeyUp: function( keyCode, event ) {
        if (keyCode == cc.KEY.enter) {
            cc.audioEngine.stopMusic( res.GameOver );
            cc.director.runScene(new MainScene());
        }
    },
    
    update: function(dt) {

    }
});

MainMenu.DIR = {
    START: 1,
}

var Endscene = cc.Scene.extend({
   onEnter: function() {
       this._super();
       var layer = new EndScene();
       layer.init();
       this.addChild( layer );
   }
});