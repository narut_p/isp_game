var CloudSmall = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/CloudSmall.png');
    },
    
    update: function(dt) {
        var posCloud = this.getPosition();
        this.setPosition(new cc.p(posCloud.x + 0.2, posCloud.y));
        if(posCloud.x > 910) {
           this.setPosition( new cc.p(0, 200 + Math.floor(Math.random() * (screenHeight-200))));
        }    
    },
});