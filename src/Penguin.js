var live = 3;
var Penguin = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.direction = Penguin.DIR.UP; 
        this.initWithFile( 'res/penguin.png' );
        
    },
    
    
    update: function(dt) {

    var pos = this.getPosition();
    
        if(this.direction == Penguin.DIR.RIGHT) {
           if(pos.x<screenWidth) {
            this.setPosition(new cc.p(pos.x+5,pos.y));
               this.initWithFile( 'res/Penguin/penguinRight.png') ;
            }    
        }
        else if(this.direction == Penguin.DIR.LEFT) {
            if(pos.x>0) {
            this.setPosition(new cc.p(pos.x-5,pos.y));
                this.initWithFile( 'res/Penguin/penguinLeft.png');
            } 
        }

        
       },
    
    switchDirection: function(num) {
        if( num == Penguin.DIR.RIGHT ) {
	    this.direction = Penguin.DIR.RIGHT;
        
	    }
        else if(num== Penguin.DIR.LEFT){
	    this.direction = Penguin.DIR.LEFT;
	    }
        else {
            this.direction = Penguin.DIR.UP;
        }
    }
});

Penguin.DIR = {
    
    RIGHT:1,
    LEFT:2,
    SPACE:3
    
};

    