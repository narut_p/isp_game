var Instruction = cc.Layer.extend({
    ctor: function() {
        this._super();
        this.init();
    },
    
    init: function() {
        this._super();
        var bg = cc.Sprite.create( res.instructionImg );
        bg.setPosition( new cc.p( 400, 300 ));
        this.addChild( bg );
 
        this.scheduleUpdate();
        this.addKeyboardHandlers();
        
        return true;
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    
    onKeyDown: function( keyCode, event ) {
        
    },
    
    onKeyUp: function( keyCode, event ) {
        if (keyCode == cc.KEY.enter) {
            cc.audioEngine.stopMusic( res.MainmenuMusic );
            live = 3;
            score = 0;
            cc.director.runScene(new StartScene());
        }
    },
    
    update: function(dt) {
        
    }
});

MainMenu.DIR = {
    START: 1,
}

var InstructionScene = cc.Scene.extend({
   onEnter: function() {
       this._super();
       var layer = new Instruction();
       layer.init();
       this.addChild( layer );
   }
});