var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.p( 0, 0 ) );
        this.bombArray = [];
        this.cloudSmallArray = [];
        this.cloudLargeArray = [];
        this.createBackground();
        this.numStage = 0;
        this.state = 0;
        this.lives = [];
        
       
        this.createPenguin();
        this.initSound();
        
        this.setLive( this.lives );
        this.createInitBomb();
        
        this.createFruit();
        this.createDragon();
        this.createCloud();
        this.createLive();
        this.scoreBoard = new ScoreBoard();
        this.scoreBoard.setPosition( cc.p( 0, 0 ));
        this.addChild( this.scoreBoard );
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        
        return true;
    },
    
    initSound: function() {
        cc.audioEngine.playMusic( res.MainMusic );
    },
    
    stage: function() {
        if( score < 5 ) {
            this.numStage = 5;  
        }
        else if( score >= 5 && score <= 10) {
            this.numStage = 6;
        }
        else if( score > 10 && score <= 20 ) {
            this.numStage = 7;
        }
        else if( score > 20 && score <= 25 ) {
            this.numStage = 8;
        }
        
    },


/** Add key */
    onKeyUp: function( keyCode, event ) {
	   
    },
    
    onKeyDown: function( keyCode, event ) {
        if ( keyCode == cc.KEY.right ) {
            this.penguin.switchDirection(1);
            
        }
        else if( keyCode == cc.KEY.left) {
            this.penguin.switchDirection(2);
            
        }
            else if(keyCode == cc.KEY.space) {
            this.penguin.switchDirection(3);
        }
    },

    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            },
        }, this);
    },
    
    update: function() {
        
        if( live == 0 ) {
            cc.audioEngine.stopMusic( res.MainMusic );
            cc.director.runScene(new Endscene());
            
        }
        this.scoreBoard.scheduleUpdate();
        this.bomb.hit( this.penguin );
        //hit
        this.banana.hit( this.penguin );
        this.apple.hit( this.penguin );
        this.apple2.hit( this.penguin );
        this.berry.hit( this.penguin );
       
        this.stage();
        if( this.numStage == 5 && this.state == 0) {
            this.createBomb();
            this.state++;
        }
        else if( this.numStage == 6 && this.state == 1) {
            this.createBomb();  
            this.state++;
        }
        else if( this.numStage == 7 && this.state == 2) {
            this.createBomb();
            this.state++;
        }
        else if( this.numStage == 8 && this.state == 3) {
            this.createBomb();
            this.state++;
        }
        
        for( var i = 0 ; i < this.numStage ; i++ ) {
            this.bombArray[i].hit( this.penguin );
        }
        this.grape.hit( this.penguin );
    },
    
    apple: function() {
        this.apple = new Apple();
        this.apple.setPosition( new cc.p(Math.floor(Math.random() * screenWidth), Math.floor(Math.random() * 800)));
        this.addChild( this.apple );
        this.apple.scheduleUpdate();
    },
    
    apple2: function() {
        this.apple2 = new Apple2();
        this.apple2.setPosition( new cc.p(Math.floor(Math.random() * screenWidth), Math.floor(Math.random() * 800)));
        this.addChild( this.apple2 );
        this.apple2.scheduleUpdate();
    },
    
    banana: function() {
        this.banana = new Banana();
        this.banana.setPosition( new cc.p(Math.floor(Math.random() * screenWidth), Math.floor(Math.random() * 800)));
        this.addChild( this.banana );
        this.banana.scheduleUpdate();
    },
    
    grape: function() {
        this.grape = new Grape();
        this.grape.setPosition( new cc.p(Math.floor(Math.random() * screenWidth), Math.floor(Math.random() * 800)));
        this.addChild( this.grape );
        this.grape.scheduleUpdate();
    },
    
    berry: function() {
        this.berry = new Berry();
        this.berry.setPosition( new cc.p(Math.floor(Math.random() * screenWidth), Math.floor(Math.random() * 800)));
        this.addChild( this.berry );
        this.berry.scheduleUpdate();
    },
    
    createCloud: function() {
        //Cloud
        for( var i = 0 ; i < 3 ; i++ ) {
            var cloudSmall = new CloudSmall();
            var cloudLarge = new CloudLarge();
            cloudSmall.setPosition( new cc.p(Math.floor(Math.random() * 1000), 200 + Math.floor(Math.random() * (screenHeight - 200))));
            cloudSmall.scheduleUpdate();    
            cloudLarge.setPosition( new cc.p(Math.floor(Math.random() * 1000), 200 + Math.floor(Math.random() * (screenHeight - 200 ))));
            cloudLarge.scheduleUpdate();
            this.cloudLargeArray.push( cloudLarge );
            this.cloudSmallArray.push( cloudSmall );
            this.addChild( this.cloudSmallArray[i] );
            this.addChild( this.cloudLargeArray[i] );
        }
    },
    
    createPenguin: function() {
        this.penguin = new Penguin();
        
        this.penguin.setPosition( new cc.p(200,45));
        this.addChild( this.penguin);
        this.penguin.scheduleUpdate();
    },
    
    createDragon: function() {
        this.dragon = new Dragon();
        this.dragon.setPosition( new cc.p(600,450));
        this.addChild( this.dragon);
        this.dragon.scheduleUpdate();
       
    },
    
    createFruit: function() {
        this.apple();
        this.apple2();
        this.banana();
        this.grape();
        this.berry();
    },
    
    createInitBomb: function() {
        this.bomb = new Bomb();
        this.bomb.setPosition( cc.p( 500, 500 ));
        this.bomb.scheduleUpdate();
        this.addChild( this.bomb ); 
    },
    
    createBomb: function() {
        for(var i = 0 ; i < this.numStage ; i++) {
            var bomb = new Bomb();
            bomb.setPosition( new cc.p(200 + Math.floor(Math.random() * (screenWidth - 200)), 200 + Math.floor(Math.random() * (screenHeight - 200))));
            bomb.scheduleUpdate(); 
            this.addChild( bomb );
            this.bombArray.push( bomb );
        }
    },
    
    createBackground: function() {
        this.background = new Background();
        this.background.setPosition( new cc.p(400, 300));
        this.addChild( this.background );
    },
    
    createLive: function() {
        for( var i = 1 ; i <= 3 ; i++ ) {
            var live = cc.Sprite.create( 'res/live.png' );
            live.setPosition( new cc.p( 50 * i, 500 ));
            this.addChild( live );
            this.lives.push( live );
        }
            return this.lives;
    },
    
    setLive: function( lives ) {
        heart = lives;
    }
    
});
    
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
    }
}); 