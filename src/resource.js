var scoreArray = [];
for( var i = 0 ; i < 10 ; i++ ) {
    scoreArray.push( 'res/Score/' + i + '.png' );   
}

var res = {
    BGMain : 'res/Mainmenu.png',
    GameOver : 'res/gameover.png',
    MainMusic : 'res/sound/main.mp3',
    MainmenuMusic : 'res/sound/mainmenu.mp3',
    catchFruit : 'res/sound/catchFruit.mp3',
    catchBomb : 'res/sound/catchBomb.mp3',
    GameOverSound : 'res/sound/gameover.mp3',
    highscoreImg : 'res/highscore.png',
    instructionImg : 'res/HTP.png'
}

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
for ( var i in scoreArray ) {
    g_resources.push(scoreArray[i]);   
}
