var score = 0;
var highscore = 0;
var scoreArray = [];
for( var i = 0 ; i < 10 ; i++ ) {
    scoreArray.push( 'res/Score/' + i + '.png' );   
}
var ScoreBoard = cc.Node.extend({
    
    ctor : function(){
        this._super();
        this.highscore = cc.Sprite.create( res.highscoreImg );
        this.highscore.setPosition( cc.p( 100, 560));
        this.addChild( this.highscore );
        
        this.first = cc.Sprite.create( scoreArray[0] );
        this.first.setPosition( cc.p(720, 560));
        this.addChild( this.first );
        this.second = cc.Sprite.create( scoreArray[0] );
        this.second.setPosition( cc.p(750, 560));
        this.addChild( this.second );
        
        this.highscoreFir = cc.Sprite.create( scoreArray[0] );
        this.highscoreSec = cc.Sprite.create( scoreArray[0] );
        this.highscoreFir.setPosition( cc.p(220, 560));
        this.highscoreSec.setPosition( cc.p(250, 560));
        this.addChild( this.highscoreFir );
        this.addChild( this.highscoreSec );
        
        if( score >= highscore ) {
            check = true;   
        }
        else {
            check = false;   
        }
        
    },
    
    update : function(dt) {
        var Seconddigit = score % 10
        var Firstdigit = Math.floor(( score / 10 ) % 10);
        var highscoreDigit2 = highscore % 10;
        var highscoreDigit = Math.floor(( highscore / 10 ) % 10);
        
        this.first.initWithFile( scoreArray[Firstdigit] );
        this.second.initWithFile( scoreArray[Seconddigit] );
       
        this.highscoreFir.initWithFile( scoreArray[highscoreDigit] );
        this.highscoreSec.initWithFile( scoreArray[highscoreDigit2] );
    },
    
    
    
});